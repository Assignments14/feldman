<?php

namespace App\Observers;

use App\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UrlObserver
{
    /**
     * Handle the url "saving" event.
     *
     * @param  \App\Url $url
     * @return void
     */
    public function saving(Url $url): void
    {
        $url->url = $this->checkScheme($url->url);
    }

    /**
     * Adds scheme part if not exists
     *
     * @param string $url
     * @return string
     */
    private function checkScheme(string $url): string
    {
        $scheme = parse_url($url, PHP_URL_SCHEME);
        Log::info($scheme);
        if (! $scheme) {
            return "http://$url";
        }
        return $url;
    }
}
