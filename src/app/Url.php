<?php

namespace App;

use App\Events\UrlCreating;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    public $timestamps = true;

    protected $dates = ['created_at', 'expires_at'];

    protected $fillable = [
        'url', 'alias', 'expires_at',
    ];
}
