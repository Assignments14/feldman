<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response as IlluminateResponse;
use Response;

trait Restable
{
    /**
     * The default status code.
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Will return a response.
     *
     * @param array $data    The given data
     * @param array $headers The given headers
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response
     */
    public function respond($data, array $headers = []): JsonResponse
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Getter for the status code.
     *
     * @return int The status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Setter for the status code.
     *
     * @param int $statusCode The given status code
     */
    public function setStatusCode($statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * Will result in a 201 code.
     *
     * @param string $message
     * @param array $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the message
     */
    protected function respondCreated(string $message, array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_CREATED);

        return $this->respondWithSuccess($message, $headers);
    }

    /**
     * Will result in an success message.
     *
     * @param string $message
     * @param array $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the message
     */
    public function respondWithSuccess(string $message, array $headers = []): JsonResponse
    {
        return $this->respond([
            'success' => [
                'message'     => $message,
                'status_code' => $this->getStatusCode(),
            ],
        ], $headers);
    }

    /**
     * Will result in a 400 error code.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error code
     */
    protected function respondBadRequest(string $message = 'Bad request', array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);

        return $this->respondWithError($message, $headers);
    }

    /**
     * Will result in an error.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error message
     */
    public function respondWithError(string $message, array $headers = []): JsonResponse
    {
        return $this->respond([
            'error' => [
                'message'     => $message,
                'status_code' => $this->getStatusCode(),
            ],
        ], $headers);
    }

    /**
     * Will result in a 401 error code.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error code
     */
    protected function respondUnauthorized(string $message = 'Unauthorized', array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_UNAUTHORIZED);

        return $this->respondWithError($message, $headers);
    }

    /**
     * Will result in a 403 error code.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error message
     */
    protected function respondForbidden(string $message = 'Forbidden', array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_FORBIDDEN);

        return $this->respondWithError($message, $headers);
    }

    /**
     * Will result in a 404 error code.
     *
     * @param string $message The given message
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error message
     */
    protected function respondNotFound(string $message = 'Not found'): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND);

        return $this->respondWithError($message);
    }

    /**
     * Will result in a 405 error code.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error message
     */
    protected function respondNotAllowed(string $message = 'Method not allowed', array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_METHOD_NOT_ALLOWED);

        return $this->respondWithError($message, $headers);
    }

    /**
     * Will result in a 422 error code.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error code
     */
    protected function respondUnprocessableEntity(string $message = 'Unprocessable', array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);

        return $this->respondWithError($message, $headers);
    }

    /**
     * Will result in a 429 error code.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error message
     */
    protected function respondTooManyRequests(string $message = 'Too many requests', array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_TOO_MANY_REQUESTS);

        return $this->respondWithError($message, $headers);
    }

    /**
     * Will result in a 500 error code.
     *
     * @param string $message The given message
     * @param array  $headers The headers that should be send with the JSON-response
     *
     * @return \Illuminate\Http\JsonResponse The JSON-response with the error message
     */
    protected function respondInternalError(string $message = 'Internal Error', array $headers = []): JsonResponse
    {
        $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);

        return $this->respondWithError($message, $headers);
    }
}