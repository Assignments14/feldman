<?php

namespace App\Http\Controllers;

use App\Url;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

final class RedirectController extends Controller
{
    public function goTo(string $alias)
    {
        $url = Url::whereAlias($alias)->first();

        if (! $url) {
            return view('error', ['error' => 'Link not found']);
        }

        if ($url->expires_at < (new Carbon())->now()) {
            return view('error', ['error' => 'Link is expired']);
        }

        return redirect($url->url);
    }

}
