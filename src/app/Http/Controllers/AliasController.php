<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShortUrlRequest;
use App\Url;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class AliasController extends BaseController
{
    public function create(ShortUrlRequest $request): JsonResponse
    {
        try {
            $url = new Url();
            $url->url = $request->url;
            $url->alias = str_random(6);
            $url->client_ip = $request->ip();

            $url->expires_at = (new Carbon())->now()->addYear(1);
            if ((int) $request->expires_min > 0) {
                $url->expires_at = (new Carbon())->now()->addMinute($request->expires_min);
            }

            $url->save();
            return $this->respondCreated(env('APP_URL') . '/' . $url->alias);
        } catch (\Exception $e) {
            return $this->respondUnprocessableEntity($e->getMessage());
        }
    }
}
