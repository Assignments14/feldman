@extends('layouts.base')

@section('content')
    <div class="title m-b-md">
        {{ $error }}
    </div>

    <div class="links">
        <a href="/">Home</a>
    </div>
@endsection