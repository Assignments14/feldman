@extends('layouts.base')

@section('content')
    <div id="div-front" class="container">
        <div class="title m-b-md">
            Enter URL to trim
        </div>
        <div>
            <input id="i-url" type="text">
        </div>

        <div class="label-sm m-b-md">
            It will expire in (min)
        </div>
        <div class="m-b-40">
            <input id="i-expires-min" type="text" value="0">
        </div>

        <div>
            <button id="b-trim" class="b-default">Trim</button>
        </div>
    </div>

    <div id="div-result" class="container label-sm display-none">
        <a id="a-link-trimmed" href="" target="_blank"></a>
        <div class="links">
            <a href="/">Trim more</a>
        </div>
    </div>
    <script>

        const AUTH_DATA = {
            'grant_type': 'password',
            'username': 'admin@test.com',
            'password': 'Test@123',
            'client_id': "<?= env('API_CLIENT_ID'); ?>",
            'client_secret': "<?= env('API_CLIENT_SECRET'); ?>"
        }

        /*
            Helpers
         */

        let token = '';

        function ajaxPOST(url, token, payload, callback) {
            const request = new XMLHttpRequest();
            request.open('POST', url, true);
            request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
            request.setRequestHeader('Accept', 'application/json');
            if (token) {
                request.setRequestHeader('Authorization', `Bearer ${token}`);
            }

            request.onload = function() {
                if (request.status >= 200 && request.status < 400) {
                    let data = JSON.parse(request.responseText);
                    callback(data);
                } else {
                    console.error('ajax error:', request.status, request.statusText);
                }
            };

            request.onerror = function() {
                console.error('Server connection error');
            };

            request.send(payload);
        }

        function ready(onreadyCb) {
            if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
                onreadyCb();
            } else {
                document.addEventListener('DOMContentLoaded', onreadyCb);
            }
        }

        /*
            Let's go!
         */
        ready(function () {

            // get access token
            ajaxPOST('/oauth/token', null, JSON.stringify(AUTH_DATA), function (response) {
                console.log('token data:', response);
                token = response.access_token;
            })

            // get url alias
            document.getElementById('b-trim').addEventListener('click', function () {
                const url = document.getElementById('i-url').value;
                if (! url) {
                    alert('Empty url field');
                }

                let expires_min = document.getElementById('i-expires-min').value;
                expires_min = expires_min ? expires_min : 0;

                console.log('trim data:', url, expires_min);

                ajaxPOST('/api/short', token, JSON.stringify({
                    'url': encodeURI(url),
                    'expires_min': expires_min
                }), function (response) {
                    console.log('response:', response);
                    if (response.success) {
                        document.getElementById('div-front').style.display = 'none';

                        const a = document.getElementById('a-link-trimmed');
                        a.href = response.success.message;
                        a.innerText = response.success.message;

                        document.getElementById('div-result').classList.remove('display-none');
                    }
                });
            });
        });
    </script>
@endsection

