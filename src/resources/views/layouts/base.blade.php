<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>URL Trimmer</title>

    <!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">--}}
    <link href="https://fonts.googleapis.com/css?family=Special+Elite" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            /*font-family: 'Nunito', sans-serif;*/
            font-family: 'Special Elite', cursive;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 14px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        input[type=text] {
            width: 560px;
            font-size: 18px;
            font-family: 'Special Elite', cursive;
            padding: 14px;
            text-align: center;
        }

        .label-sm {
            margin-top: 40px;
            font-size: 36px;
        }

        .m-b-40 {
            margin-bottom: 40px;
        }

        .b-default {
            display: inline-block;
            padding: 0.3em 1.2em;
            margin: 0 0.3em 0.3em 0;
            border-radius: 8px;
            box-sizing: border-box;
            text-decoration: none;
            font-weight: 400;
            font-size: 24px;
            color:#FFFFFF;
            background-color: #4eb5f1;
            text-align: center;
            transition: all 0.2s;
        }

        .display-none {
            display: none;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content">
        @yield('content')
    </div>
</div>
</body>
</html>

