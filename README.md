#### Feldman's Trimmer. Up and Running

In the project root folder run:
```
docker-compose build
```
```bash
docker-compose run -d
``` 
In the src/ folder run:
```bash
php artisan migrate --seed && php artisan passport:install
```

Set API_CLIENT_ID and API_CLIENT_SECRET from the previous command output:
```bash
Password grant client created successfully.
Client ID: 2 // API_CLIENT_ID
Client Secret: i3H10pCoKs1Jg3Vz62zLZB65JYQWRrBrnaPnr052 // API_CLIENT_SECRET
```

Go to url in your browser:
```bash
http://localhost:8000
```